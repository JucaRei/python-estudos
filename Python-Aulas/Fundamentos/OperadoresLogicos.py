# -*- coding: utf-8 -*-
#%%

print(True or False) # Ou   
print(7 != 3 and 2 > 3) # E

#Tabela verdade do AND (e)
print(True and True)
print(True and False)
print(False and True)
print(False and False)
print(True and True and False and True and True and True and True and True)

#Tabela Verdade do OR (ou)
print(True or True)
print(True or False)
print(False or True)
print(False or False)
print(True or True or False or True or True or True or True)

#Tabela Verdade do XOR  (ou exclusivo, ou 1 ou outro)
print(True != True)
print(True != False)
print(False != True)
print(False != False)
print(True != True != False != True != True != True != True)

#Tabela Verdade Negação(unario)
print(not True)
print(not False)

print(not 0)
print(not 1)
print(not not 1)

#Cuidado!  Operador bit a bit
print(True & True)
print(False | False)
print(True ^ False)

# AND bit-a-bit
# 3 = 11
# 2 = 10
# _ = 10
print(3 & 2)

# OR bit-a-bit
# 3 = 11
# 2 = 10
# _ = 11
print(3 | 2)

# XOR bit-a-bit
# 3 = 11
# 2 = 10
# _ = 01
print(3 ^ 2) 

#Um pouco de realidade
saldo = 1000
salario = 4000
despesas = 2967

saldo_positivo = saldo > 0
despesas_controladas =  salario - despesas >= 0.2 * salario
meta = saldo_positivo and despesas_controladas
print(meta)

# %%
# Desafio Operadores Lógicos

# Os Trabalhos
trabalho_terca = True
trabalho_quinta = False

"""
- Confirmando os 2: TV 50' + Sorvete
- Confirmando apenas 1: TV 32' + Sorvete
- Nenhum confirmado: Fica em casa
"""
tv_50 = trabalho_terca and trabalho_quinta
sorvete = trabalho_terca or trabalho_quinta
tv_32 = trabalho_terca != trabalho_quinta  # xor
mais_saudavel = not sorvete

print("Tv50={} Tv32={} Sorvete={} Saudável={}"
      .format(tv_50, tv_32, sorvete, mais_saudavel))

# o formart para cada par de chaves, voce pode ter um valor associado 
# "{}, {} = {}".format(1, False, 'resultado')