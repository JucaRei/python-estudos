# -*- coding: utf-8 -*-

#%%

print("Teste de programa 2")
print(80 * 90)
print(1 \
    +2)

# Tipos Basicos
print(True)
print(False)
print(1.2 + 1)
print('Tipo string , qualquer lingua')
print("Tanto aspas duplas, quanto simples, você decide")
print("Você eh " + 3 * 'muito ' + 'LEGAL!')
#print(3 + '3')  -> ambiguidade
print([1,2,3])      #Listas em python eh muito parecido, como array(indexada) em outras linguagens; so que no python a lista eh dinâmica(você consegue ir acrescentando ela)
print({'nome': 'Pedro', 'idade': 22})   # dicionario eh uma estrutura chave e valor, em python


print('-------------------------------------------------------')
lista = [1,2,3]
dicionario = ({'nome': 'Pedro', 'idade': 22})
print(lista, dicionario)
print(None) #equivalente ao null , undefined

