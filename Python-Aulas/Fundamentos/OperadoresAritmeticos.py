# -*- coding: utf-8 -*-
#%%

print (2 + 3)
print (4 - 7)
print (2 * 5.3)
print (9.4 / 3) #dizimo
print (9.4 // 3)  # sem dizimo
print (2 ** 8) # elevado a potência
print (10 % 3) # modulo eh o resto da divisão, no caso 10 divido por 3, o resto sera 1

a = 12
b = a
print (a + b)

print("----------------------------------------------")
print("DESAFIO Percentual gasto por mês")

salario = 3459.45
despesas = 2456.2
percentual_comprometido = despesas / salario * 100
print(percentual_comprometido)