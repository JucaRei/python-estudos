# -*- coding: utf-8 -*-
#%%
print(3 > 4)
print(3 >= 4)
print(1 < 2)
print(3 <= 1)
print(3 != 2)
print(3 == 3)
print(2 == '2')


""" Operadores de Atribuição """

a = 3
a = 3 + 7
print(a)

# a = 5
a +=5 # a = a + 5
print(a)

a -= 3 # a = a - 3
print(a)

a /= 4
print(a)

a %= 4
print(a)

a **= 8
print(a)

a //= 256
print(a)