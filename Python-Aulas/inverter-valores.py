a = 7


b = 3


a, b = b, a  # a vai receber o valor b ; e b vai receber o valor de a , invertendo os valores
print(a, b)


# outro jeito de fazer, mais longo

c = 10
d = 5

temp = c  # temp armazena o valor de c
c = d   # c recebe o valor de d
d = temp  # d recebe o valor de temp , que era o antigo valor de c , invertendo os valores

print(c, d)

# Estrutura de Dados : Nó , lista , fila, pilha
# dicionario em python é uma estrutura chave/valor
