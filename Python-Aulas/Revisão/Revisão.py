a = [1, 3, 5, 7, 9]
acumlulador = 0
for i in set(a) - {5}:      # {5} conjuntos
    acumlulador += i

print(f"O valor do 'acumulador' é %d" % acumlulador)

# ==================================================================

# x = input("Digite um valor para a variável x:")
x = 50
print("O valor de x é", x)
print("O tipo de x é", type(x))

# ==================================================================

N = 10
lista = [0, ] * N
for i in range(N):
    lista[i] = int(input("Digite o valor da posição %d:" % (i+1)))
print(f"Lista de valores:", lista)
