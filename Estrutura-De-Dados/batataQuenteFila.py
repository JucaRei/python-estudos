class Fila:
    def __init__(self, itens=[]):
        self._itens = itens

    def isEmpty(self):
        return self._itens == []

    def add(self, item):
        self._itens.insert(0, item)

    def remove(self):
        return self._itens.pop()

    def size(self):
        return len(self._itens)

    def topo(self):
        return self._itens[len(self._itens)-1]

    def __str__(self):
        return "{}" .format(self._itens)

