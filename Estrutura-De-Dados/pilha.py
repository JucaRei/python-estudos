# -*- coding: UTF-8 -*-

def push(p, elemento, tipo=str):
    if type(elemento) == tipo:
        p.append(elemento)

def pop(p):
    return p.pop() if len(p) > 0 else None

def palindromo(s):
    lista = list(s)

    #Cria a pilha vazia
    pilha = []

    #Insere metade da lista de letras na pilha
    for letra in lista[:len(lista)//2]:
        push(pilha, letra)

    proxima_letra = len(lista)//2 if len(lista)%2 == 0 else len(lista)//2 + 1

    for letra in lista[proxima_letra:]:
        if letra != pop(pilha):
            return False

    return True
    

def balanceada(s):
    abre = ['{', '[', '(']
    fecha = ['}', ']', ')']
    relacao = {'{':'}', '[':']', '(':')'}

    lista = list(s)
    pilha = []

    for c in lista:
        if c in abre:
            push(pilha, c)
        elif c in fecha:
            topo_pilha = pop(pilha)
            if topo_pilha is None or relacao[topo_pilha] != c:
                return False

    return len(pilha) == 0

if __name__ == '__main__':
    testes = ['oi', 'xaa', 'oio', 'aa', 'p', 'abcdedcba', 'abcdeedcba']

    for teste in testes:
        if palindromo(teste):
            print("%s é um palíndromo"%teste)
        else:
            print("%s não é um palíndromo"%teste)

    print("\n\n")

    testes = ['()', '({)}', '(kbfwf)[]', '(([])())', '((', '([][]{()})']

    for teste in testes:
        if balanceada(teste):
            print("%s está balanceada"%teste)
        else:
            print("%s não está balanceada"%teste)