#coding: utf# -*- coding: UTF-8 -*-

""" Cada grupo deve entregar 1 arquivo busca.py contendo as seguintes funções: """

def verifica_ordenacao(lista):
    """Verifica se a lista está ordenada e retorna True se a lista estiver ordenada ou False se a lista não tiver ordenada"""
pass


def busca(lista, elemento_desejado):
    """Busca linear que retorna o índice do elemento desejado se estiver na lista, caso contrário retorna None"""
pass


def busca_binaria(lista, elemento_desejado):
    """Busca binária que retorna o índice do elemento desejado se estiver na lista, caso contrário retorna None"""
pass



# Escrevam o nome e o número de matrícula de cada membro do grupo como um comentário nas primeiras linhas do arquivo busca.py. """

"""Busca binária com uma lista de strings que retorna o índice do elemento desejado se estiver na lista, caso contrário retorna None. Se a lista não estiver ordenada, também retorna None"""


def pesquisa_binaria1(lista, item):
    numero_mais_baixo = 0  # lembrando que 1 array sempre começa do indice 0
    # como o indice de 1 array começa de 0, então o ultimo indice é o total -1 (por ex: 1 à 100, indice 100 - 1 = 99, ultimo indice é 99)
    numero_mais_alto = len(lista) - 1

    while numero_mais_baixo <= numero_mais_alto:
        meio_da_lista = (numero_mais_baixo + numero_mais_alto) / 2
        chute = lista[meio_da_lista]
        if chute == item:
            return meio_da_lista
        if chute > item:
            numero_mais_alto = meio_da_lista - 1
        else:
            numero_mais_baixo = meio_da_lista + 1
    return None


minha_lista = ['abc', 'abd', 'bola', 'casa', 'casas']
