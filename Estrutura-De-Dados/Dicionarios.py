# coding: utf# -*- coding: UTF-8 -*-

# usando o dict
moedas = dict([
    ('Brasil', 'Real'),
    ('EUA', 'Dólar'),
    ('Inglaterra', 'Libra'),
    ('Portugal', 'Euro'),
    ('China', 'Yuan'),
    ('Espanha', 'Euro'),
    ('Grécia', 'Euro')
])
print("A moeda do Brasil é o", moedas['Brasil'])

# usando chaves
moedas = {
    'Brasil': 'Real',
    'EUA': 'Dólar',
    'Inglaterra': 'Libra',
    'Portugal': 'Euro',
    'China': 'Yuan',
    'Espanha': 'Euro',
    'Grécia': 'Euro'
}
print("A moeda do Brasil é o", moedas['Brasil'])

# acessar a lista de chaves usando keys
moedas = {
    'Brasil': 'Real',
    'EUA': 'Dólar',
    'Inglaterra': 'Libra',
    'Portugal': 'Euro',
    'China': 'Yuan',
    'Espanha': 'Euro',
    'Grécia': 'Euro'
}
print("Lista de Países", list(moedas.keys()))

# acessar a lista de chaves usando keys
moedas = {
    'Brasil': 'Real',
    'EUA': 'Dólar',
    'Inglaterra': 'Libra',
    'Portugal': 'Euro',
    'China': 'Yuan',
    'Espanha': 'Euro',
    'Grécia': 'Euro'
}
print("Lista de Países", list(moedas.values()))

# modelo tabela

alunos_notas = {
    2019001: [6, 3, 8, 8, 9],
    2019002: [5, 4, 7, 7, 9],
    2019003: [8, 2, 7, 8, 7],
    2019004: [5, 5, 6, 9, 9],
    2019005: [7, 2, 8, 10, 9],
    2019006: [9, 6, 8, 9, 9],
    2019007: [8, 7, 8, 10, 9],
    2019008: [6, 2, 4, 5, 8],
    2019009: [5, 5, 9, 8, 7],
    2019010: [6, 4, 7, 9, 9],
    2019011: [8, 2, 6, 9, 9],
}

# modelo de árvore

geografia = {
    "Terra": {
        "Ásia": {
            "Japão": {},
            "China": {},
            "Vietnam": {}
        },
        "Europa": {
            "Portugal": {},
            "Espanha": {},
            "Alemanha": {}
        },
        "Ámerica": {...},
        "África": {...},
        "Oceania": {...},
        "Antártida": {...}
    }
}

# modelo lista

presidentes = {
    "EUA": "D. Trump",
    "Brasil": "J. Bolsonaro",
    "Argentina": "M. Macri",
    "Chile": "S. Piñera",
    "Peru": "M. Vizcarra"
}