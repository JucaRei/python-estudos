# -*- coding: UTF-8 -*-


class Queue:

    # Construtor
    def __init__(self):
        self.queue = list()
        self.maxSize = 26  # tamanho maximo da fila
        self.head = 0  # inicio da fila
        self.tail = 0  # final da fila

    # Adding elements
    def enqueue(self, data):
        # Checking if the queue is full
        if self.size() >= self.maxSize:
            return ("Fila Cheia")
        self.queue.append(data)
        self.tail += 1
        return True

    # Deletando elementos
    def dequeue(self):
        # Checa se a fila está vazia
        if self.size() <= 0:
            self.resetQueue()
            return ("Fila vazia")
        data = self.queue[self.head]
        self.head += 1
        return data

    # Calcular o tamanho
    def size(self):
        return self.tail - self.head

    # Resetar a fila
    def resetQueue(self):
        self.tail = 0
        self.head = 0
        self.queue = list()

    def show(self):
        for i in self.queue:
            print(i)


q = Queue()
print(q.enqueue('a'))  # prints True
print(q.enqueue('b'))  # prints True
print(q.enqueue('c'))  # prints True
print(q.enqueue('d'))  # prints True
print(q.enqueue('e'))  # prints True
print(q.enqueue('f'))  # prints True
print(q.enqueue('g'))  # prints True
print(q.enqueue('h'))  # prints True
print(q.enqueue('i'))  # prints Queue Full!
print(q.size())  # prints 8
print(q.dequeue())  # prints 8
print(q.dequeue())  # prints 7
print(q.dequeue())  # prints 6
print(q.dequeue())  # prints 5
print(q.dequeue())  # prints 4
print(q.dequeue())  # prints 3
print(q.dequeue())  # prints 2
print(q.dequeue())  # prints 1
print(q.dequeue())  # prints Queue Empty
# Queue is reset here
print(q.enqueue('a'))  # prints True
print(q.enqueue('b'))  # prints True
print(q.enqueue('c'))  # prints True
print(q.enqueue('d'))  # prints True
print(q.show())
