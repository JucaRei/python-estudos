# -*- coding: UTF-8 -*-

""" 
1. Implementar uma fila tal que os elementos {a, b, c, ..., z} são enfileirados de forma intercalada da seguinte forma:
enfileira(fila, ‘a’) # primeiro elemento da sequência enfileira(fila, ‘z’)
# último elemento da sequência enfileira(fila, ‘b’) # segundo elemento da sequência enfileira(fila, ‘y’) # penúltimo elemento da sequência ...
Apresente o código que implementa este enfileiramento.
Crie uma função que retorne uma lista com os p últimos elementos enfileirados sem modificar a fila.

def  ultimos(fila, p):  
    pass

1.	letras = [‘a’, ‘b’, ‘c’, ‘d’, ‘e’]
fila = []
enqueue(fila, letras[0])
enqueue(fila, letras[4])
enqueue(fila, letras[1])
enqueue(fila, letras[3])
enqueue(fila, letras[2])

ultimos_elementos = ultimos(fila, 3)
print(ultimos_elementos) # mostra os últimos elementos enfileirados: [‘b’, ‘d’, ‘c’]
print(fila) # mostra a fila como estava antes: [‘a’, ‘e’, ‘b’, ‘d’, ‘c’]
"""

letras = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','x','y','w','z']
fila = []
comeco = letras[::2]
ultimo = letras[::-2]
contador = 0

print(comeco)
print(ultimo)
print(ultimo[::-1])
print(len(comeco))
print(len(ultimo))

print('===================================================')


for i in letras[::2]:
    print(i)

print('===================================================')


lista = [lis for lis in letras[::2]]
print(lista)

enqueue(lista, letras[0])
# def comeco(lista, letras):
#     lista = [lis for lis in letras[::2]]
#     return lista    


# def comeco (lista, letras):
#     lista = letras
#     if (len(lista) % 2 == 1):
#         return letras
#     else:
#         return letras


print('===================================================')


for i in range(len(letras[::2])):
    print(i)
    

# def ultimos(fila, p):
#     for x in fila:



""" 
class FilaIntercalada:
    

2. Apresente uma função para o Problema Josephus, ou Jogo da Batata-Quente. Este problema consiste em termos uma roda de N jogadores [0, 1, 2, ..., N - 1] e uma bola. A bola começa nas mãos de um dos jogadores e avança P jogadores. O jogador que ficar com a bola na mão sai da roda. O último jogador restante é o vencedor.
def  josephus(jogadores, p): 
    pass
A função deve retornar o jogador vencedor e uma lista com a ordem em que os jogadores foram eliminados.

"""

# class Fila:
#     def __init__(self):
#         self.fila = []
#     def inserir(self, n):
#         self.fila.append(n)  
#     def excluir(self):
#         if not self.vazia():  
#             return self.fila.pop(0)  
#     def tamanho(self):
#         return len(self.fila)  
#     def vazia(self):
#         return self.tamanho() == 0  
'''
lista2 = list(range(1000))

#lista3 = [x*2 for in x range(1000)]

for x in lista2:
    print(x)


for x in lista2[40:60]:
    print(x)

lista = range(0, 10)
intercaladas = lista[::2], lista[1::2]
print(intercaladas)
'''