# -*- coding: UTF-8 -*-
from batataQuenteFila import Fila


def batata_quente(criancas, num):
    fila = Fila()
    for nome in criancas:
        fila.add(nome)
    while fila.size() > 1:
        for i in range(num):
            fila.add(fila.remove())
        fila.remove()
    return fila.remove()


lista = ["Carlos", "Joao", "Maria", "Julia", "Tadeu", "Lara"]
print(batata_quente(lista, 3))
