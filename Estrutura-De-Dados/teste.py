# -*- coding: UTF-8 -*-

x = 3
x += 10
print("O valor de x é %d" % x)

# o % pega o conteudo de variavel(no caso x)
# o d indica o formato da variavel (d variavel inteira decimal)

a = 3
a += 10
if a > 15:
    print("variável 'a' é  maior que 15")
else:
    print("variável 'a' é menor ou igual a 15")


b = 3

while b < 10:
    b += 3
print("O valor de 'a' maior que 10 é %d" % b)

c = [1, 3, 4, 5, 6, 8]
n = len(c)
acumulador = 0
for i in range(n):
    acumulador += c[i]
print("O valor acumulado é %d" % acumulador)

d = [1, 2, 3, 4, 5, 6, 7, 8, 9]
acumulador1 = 0
for i in d:
    acumulador1 += i
print("O valor 'acumulador' é %d" % acumulador1)

# somar todos os elementos diferente de 5

e = [1, 3, 5, 7, 9]
acumulador2 = 0
for i in e:
    if i != 5:
        acumulador2 += i
print("O valor 'acumulador' é %d" % acumulador2)

f = [1, 3, 5, 7, 9]
acumulador3 = 0
for i in f:
    acumulador3 += i if i != 5 else 0
print("O valor 'acumulador' é %d" % acumulador3)

g = [1, 3, 5, 7, 9]
acumulador4 = 0
# set tranforma o argumento em conjunto(no caso ele retira o 5 do array)
for i in set(g) - {5}:
    acumulador4 += i
print("O valor do 'acumuldador' é %d" % acumulador4)

w = int(input("Digite o valor de w:"))
print("O valor de w é", x)
print("O tipo de é", type(x))


N = 10
lista = [0, ]*N
for i in range(N):
    lista[i] = int(input("Digite o valor da posição % d: "% (i+1)))
print("Lista de valores: ", lista)
