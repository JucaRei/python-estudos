# -*- coding: UTF-8 -*-


def fatorial_interativo(n):  # modo interativo
    if n == 0:
        return 1

    resultado = 1
    for contador in range(1, n + 1):
        resultado = resultado * contador

    return resultado


print(fatorial_interativo(3))


def fatorial_recursivo(n):
    if n == 0:
        return 1
    else:
        return n * fatorial_interativo(n - 1)  # função chamando ela mesma


print(fatorial_recursivo(6))
print('======================================================================')
''' ================================================================================='''


def fibonacci(n):
    if n == 0:
        return 0  # de fibonacci 0  retorna 0
    elif n == 1:
        return 1   # fibonacci de 1 retorna 1
    else:
        # retorna a soma dos dois ultimos
        return fibonacci(n - 1) + fibonacci(n - 2)


for i in range(20):
    print('Fibonacci (%d) = %d' % (i, fibonacci(i)))

print(fibonacci(0))
print(fibonacci(1))
print(fibonacci(2))
print(fibonacci(3))
print(fibonacci(4))
print(fibonacci(9))
print('======================================================================')

''' ================================================================================='''

''' Progressão Aritmética: PA
pa(n) = pa(n - 1) + r
pa(0) = c

r é a razão da progressão
c é o primeiro elemento da progressão 

'''
# a diferença entre dois elementos consectivos é igual a razão , ou seja sempre terão o numero da razão de diferença


def progressao_iterativa(n, c, r):
    resultado = c

    for i in range(n):
        resultado = resultado + r

    return resultado


print(progressao_iterativa(0, 1, 5))  # posicao 0 , é o anterior mais a razao
print(progressao_iterativa(1, 1, 5))
print(progressao_iterativa(2, 1, 5))
print(progressao_iterativa(3, 1, 5))
# a diferença entre os numeros sempre vai ser 5 , porque a razao é 5
print(progressao_iterativa(4, 1, 5))
print('======================================================================')

''' ================================================================================='''

""" 
Progressão Geometrica: PG   (ao contrário da PA , você vai multiplicar ao invês de somar)
pg(n) = pg(n - 1) * r
pg(0) = c

r é a razão da progressão
c é o primeiro elemento da progressão
"""


def progressao_geometrica(n, c, r):
    resultado = c

    for i in range(n):
        resultado = resultado * r

    return resultado


print(progressao_geometrica(0, 1, 5))
print(progressao_geometrica(1, 1, 5))
print(progressao_geometrica(2, 1, 5))
print(progressao_geometrica(3, 1, 5))
print(progressao_geometrica(4, 1, 5))

print('======================================================================')

''' ================================================================================='''
''' Soma de uma lista recursiva '''

# lista = (1,2,3,4,5,6,7,8,9,100)
# soma =  0

# for i in lista:
#     soma = soma + i
#     print(soma)

# def soma(lista):
#     if len(lista) == 0
#         return 0
#     else:
#         return lista[0] + soma(lista[1:])
    
# soma(lista(1,2,3,4,5,6))

# busca linear

def RecursiveSearch( searchArray, index, valueOfNumberToFind):
    
    if index >= len(searchArray):
        return -1
    
    if searchArray[index] == valueOfNumberToFind:
        return index + 1

    return RecursiveSearch(searchArray, index + 1, valueOfNumberToFind)
 

searchArray = [12, 34, 54, 2, 3]
valueOfNumberToFind = 3

print(RecursiveSearch(searchArray, 0, valueOfNumberToFind))

print('==============================================')

def busca_linear_recursiva(lista, elemento, indice = 0):
    if len(lista) == 0:   # se for igual a zero nåo tem elemento que eu estou procurando
        return None
    elif lista[0] == elemento:  # se tiver o elemento , retorna o indice
        return indice
    else:
        return busca_linear_recursiva(lista[1:], elemento, indice + 1) # 1: tira o elemento 0
    
    
if __name__ == '__main__':
    bla = [10, 34, 12, 17, 2, 90, 15]
    e = 2
    
    posicao = busca_linear_recursiva(bla, e)
    
    if posicao is None:
        print("%d esta na posição %d da lista"%(e, posicao))
        
print('==============================================')

""" Busca Binária recursiva """
def busca_binaria_recursiva(lista, elemento):
    # inicio = 0
    # fim =  len(lista)-1
    def recursivo(inicio, fim):
        meio = (inicio + fim) //2
        if inicio > fim:   # verifica se tem algum elemento , senão retorna None
            return None
        elif (lista[meio] < elemento):      # verifica se o elemento esta depois do meio da lista
            return recursivo(meio + 1, fim)
        elif (lista[meio] > elemento):      # verifica se o elemento esta antes do meio da lista
            return recursivo(inicio, meio - 1)
        else:
            return meio
    return recursivo(0, len(lista)-1)


lista = [1,2,3,4,5,6,7,8,9,10]
print(busca_binaria_recursiva(lista, 6))
print(busca_binaria_recursiva(lista, 3))