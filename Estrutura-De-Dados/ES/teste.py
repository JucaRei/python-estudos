# -*- coding: UTF-8 -*-
class Pessoa:

    def __init__(self, nome, idade):  # passa para o construtor os paramatros

        self.nome = nome        # armazenando o nome
        self.idade = idade      # armazenando idade

    def getNome(self):
        return self.nome    # retorna o nome da pessoa

    def getIdade(self):
        return self.idade   # retorna o nome da pessoa

    def setIdade(self, idade):
        self.idade = idade    # posso colocar outra idade


p1 = Pessoa('Juca', 28)
p2 = Pessoa('Junior', 20)
p3 = Pessoa('Jhonny', 18)

print('=====================================')


""" =========================================================================="""

print("Nome: %s" % p1.getNome())  # p. acessar os metodos desse classe pessoa
p1.setIdade(23)
print("Idade: %d" % p1.getIdade())

#  %s  é um especificador de formato do tipo string
#  %d é um especificador do formato inteiro

print('=====================================')

pessoas = []

# adiciona pessoa , a lista pessoas (append)
pessoas.append(p1)
pessoas.append(p2)
pessoas.append(p3)

# iterar sobre pessoas
for pessoa in pessoas:
    # iterando sobe a lista pessoas , vai mostrar todas as pessoas
    print("Nome: %s" % pessoa.getNome())
    print("Idade: %s" % pessoa.getIdade())

print('=====================================')


""" =========================================================================="""

# pares é uma lista por isso usa colchetes
impares = [i for i in range(100) if (i % 2 == 1)]  # i é cada 1 dos elementos da lista
print(impares)

# isso em uma lista eh chamada de compreenção de listas

print('=====================================')


""" =========================================================================="""

#função que retorna a potência de um número ao quadrado
def potencia2(x):
    return  x ** 2

print(potencia2(9))

#usando a expressão lambda

potencia2_ = lambda x: x**2

print(potencia2_(30))

print('=====================================')


""" =========================================================================="""

#  FATORIAL

# 5! = 5 * 4 * 3 * 2 * 1 = 120
# 3! = 3 * 2 * 1 = 6
# n! = n * (n - 1) * (n - 2)...


def fatorial(n):
    if (n == 0):
        return 1  #fatorial de 0 é 1
    else:
        return (n * fatorial(n - 1))  # é  recursivo 

print(fatorial(10))

fatorial_ = lambda n: n * fatorial(n - 1) if n > 1 else 1  # retorna 1 , por o fatorial de 0 é 1

print(fatorial_(6))
print('=====================================')

""" =========================================================================="""

'''map - usa ele quando , voce que aplicar uma função em todos os itens de 1 ou mais sequencias'''
#map

lista = [1, 2, 3, 43, 21]
m = map(lambda x: x**2, lista)  # cada elemento da lista será elevado a potência(no caso 2)
for i in m:  #percorre o m( que tem como parametros, potencia e lista)
    print(i)
    
print('=====================================')

""" =========================================================================="""
''' reduce - aplica uma função sobre uma sequencia, que vai acumulando o valor de retorno da função, a partir de um valor inicial'''
# ela foi removida do python3, para utilizar , tem que realizar 1 import do functools

import functools

print(functools.reduce(lambda x,y: x+y, [1,2,3,4]))   # ela vai acumular o valor de retorno de uma funcao a partir do valor inicial (nesse caso x + y, ela soma basicamente)
print('=====================================')

""" =========================================================================="""
'''Filter - ela filtra elementos que correspondem a um predicado '''

f = filter(lambda x: x % 2 == 0, range(167  )) #filtra elementos que são pares, nesse exemplo
for i in f:
    print(i)
    
print('=====================================')

""" =========================================================================="""