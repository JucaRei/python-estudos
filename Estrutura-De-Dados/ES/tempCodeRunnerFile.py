def busca_linear_recursiva(lista, elemento, indice = 0):
    if len(lista) == 0:   # se for igual a zero nåo tem elemento que eu estou procurando
        return None
    elif lista[0] == elemento:  # se tiver o elemento , retorna o indice
        return indice
    else:
        return busca_linear_recursiva(lista[1:], elemento, indice + 1) # 1: tira o elemento 0
    
    
if __name__ == '__main__':
    bla = [10, 34, 12, 17, 2, 90, 15]
    e = 2
    
    posicao = busca_linear_recursiva(bla, e)
    
    if posicao is None:
        print("%d esta na posição %d da lista"%(e, posicao))