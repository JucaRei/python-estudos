def busca_binaria(lista, elemento, esquerda=0, direita=-1):
    if direita == -1:
        direita = len(lista) - 1

    if esquerda > direita:
        return None

    meio = (esquerda + direita) // 2
    print("Elemento visitado =", lista[meio])

    if lista[meio] == elemento:
        return meio
    elif lista[meio] > elemento:
        direita = meio - 1
    else:
        esquerda = meio + 1
    return busca_binaria(lista, elemento, esquerda, direita)


lista = [1, 2, 3, 4, 5, 6, 7]
