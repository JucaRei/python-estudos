# instalar o pandas:
# pip install --user pandas

import pandas as pd

df = pd.read_csv(
    '/home/jucadev/Documentos/workspace/gitlab/python-estudos/Estrutura-De-Dados/ACs/AC08/cidades_br.csv')


cidades = df['Nome do Município'].tolist()
estados = df['UF'].tolist()


for cidade, estado in zip(cidades, estados):
    # print(cidade)
    pass


def cidades_por_estado(cidades):


pass

'''
    Usando o arquivo cidades_br.csv, vamos organizar os dados de 4 formas com dicionários:
- cada estado é uma chave e as cidades de cada estado em uma lista;
- listas de cidades que começam com cada letra do alfabeto;
- cada cidade é uma chave e o valor é o estado correspondente;
- uma árvore com a hierarquia: Brasil -> Estados -> Cidades.

Entregue 1 arquivo cidades.py com as quatro funções:
def cidades_por_estado(cidades):
pass
def cidades_por_letra(cidades):
pass
def estado_por_cidade(cidades):
pass
def arvore_do_brasil(cidades):
pass

OBS.: É possível usar a biblioteca Pandas para fazer a leitura do arquivo.
'''

"""
Caros, boa tarde!

Preparei um gabarito para os dicionários que vocês devem entregar na AC 8.

Utilizei a biblioteca pytest para automatizar o teste. A chamada do teste deve ser:

pytest cidades_checking.py

No arquivo pytest_resultados.txt, eu descrevi como vocês podem analisar os resultados dos testes fornecidos pelo pytest.

O teste irá funcionar somente se:
- os arquivos estiverem no mesmo diretório;
- o arquivo com as funções for cidades.py
- as funções seguirem o padrão estabelecido na descrição da AC 8.

Sugestão: abram os arquivos JSON com o bloco de notas para ver como os dicionários estão organizados.
"""
# pip install --user pandas
