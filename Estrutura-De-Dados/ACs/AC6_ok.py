#coding: utf-8

'''
Grupo:
Nome: Reinaldo Ponce Junior       RA: 1020056
Nome: Gabriel Moreira dos Santos  RA: 1800782
Nome: Willyam Oliveira Santos     RA: 1801002
Nome: Beatriz Rosália Silva       RA: 1801002
Nome: Fabio Cunha de Matos        RA: 1800748

'''
def busca_binaria(lista, elemento_desejado):
    a = 0
    c = len(lista)-1
    while a <= c:
        b = (a+c)//2
        print ('meio da busca binaria',b)
        if elemento_desejado == lista[b]:
            return b
        elif elemento_desejado > (lista[b]):
            a = b +1
        else:
            c = b -1
    return 'lista nao ordenada',None

def particiona(v, ini, fim):
	pivo = ini
	for i in range(ini + 1, fim + 1):
		if v[i] <= v[ini]:
			pivo += 1
			v[i], v[pivo] = v[pivo], v[i]

	v[pivo], v[ini] = v[ini], v[pivo]
	return pivo


def quick_sort(lista, ini, fim):
    print('primeiro elemento no quick',ini)
    if fim > ini:
        pivo = particiona(lista, ini, fim)
        quick_sort(lista, ini, pivo - 1)
        quick_sort(lista, pivo + 1, fim)
    return lista



def busca_em_listas(lista, elemento, i):
    if len(lista) == 0:
        return 0
    elif i >= len(lista):
        return None
    elif elemento == lista[i]:
        return i
    return busca_em_listas(lista,elemento, i+1)

    


lista = [50, 20, 70, 15]
elemento = 70
print(quick_sort(lista, 0, len(lista) - 1))
print(busca_binaria(lista,elemento))
print(busca_em_listas(lista,elemento,0))
