# # passa a lista, o inicio da lista e o fim da lista
# def particiona(lista, inicioVetor, finalVetor):
#     # o pivô é o elemento do ínicio
#     pivo = inicioVetor
#     for i in range(inicioVetor + 1, finalVetor + 1):
#         if lista[i] <= lista[inicioVetor]:
#             pivo += 1
#             # troca
#             lista[i], lista[pivo] = lista[pivo], lista[i]
#     lista[pivo], lista[i] = lista[i], lista[pivo]   # inverte
#     return pivo


# def quick_sort(lista, inicioVetor, finalVetor):
#     '''
#     Se o Fim for maior quer início , então eu calculo a posição do pivô
#     utilizando a função particiona
#     '''
#     if finalVetor > inicioVetor:  # particiona a lista em 2 partições
#         pivo = particiona(lista, inicioVetor, finalVetor)

#     '''
#         Tendo o pivô, chama a função duas vezes para cada partição, 
#         a primeira para os elementos que estão antes do pivô e a segunda,
#         é para os elementos que estão depois do pivô
#     '''
#     quick_sort(lista, inicioVetor, pivo - 1)
#     quick_sort(lista, pivo + 1, finalVetor)

# lista = [4,3,2,1]
# quick_sort(lista, 0, len(lista) - 1)
# print(lista)