""" Busca Binária recursiva """


def busca_binaria_recursiva(lista, elemento):
    # inicio = 0
    # fim =  len(lista)-1
    def recursivo(inicio, fim):
        meio = (inicio + fim) // 2
        if inicio > fim:   # verifica se tem algum elemento , senão retorna None
            return None
        elif (lista[meio] < elemento):      # verifica se o elemento esta depois do meio da lista
            return recursivo(meio + 1, fim)
        elif (lista[meio] > elemento):      # verifica se o elemento esta antes do meio da lista
            return recursivo(inicio, meio - 1)
        else:
            return meio
    return recursivo(0, len(lista)-1)


lista = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
print(busca_binaria_recursiva(lista, 6))
print(busca_binaria_recursiva(lista, 3))
