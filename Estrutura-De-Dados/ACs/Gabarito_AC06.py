# coding: utf# -*- coding: UTF-8 -*-

def busca_binaria(lista, elemento, esquerda=0, direita=-1):
    if direita == -1:
        direita = len(lista) - 1

    if esquerda > direita:
        return None

    meio = (esquerda + direita) // 2
    print("Elemento visitado =", lista[meio])

    if lista[meio] == elemento:
        return meio
    elif lista[meio] > elemento:
        direita = meio - 1
    else:
        esquerda = meio + 1

    return busca_binaria(lista, elemento, esquerda, direita)


def particiona(lista, esquerda, direita):
    pivo = lista[esquerda]
    lista_aux = [0, ]*(direita - esquerda + 1)

    i, j = 0, (len(lista_aux) - 1)
    for e in lista[(esquerda+1):(direita+1)]:
        if e < pivo:
            lista_aux[i] = e
            i += 1
        else:
            lista_aux[j] = e
            j -= 1

    lista_aux[i] = pivo

    lista[esquerda:(direita+1)] = lista_aux

    return esquerda + i


def quick_sort(lista, esquerda=0, direita=None):
    if direita is None:
        direita = len(lista) - 1

    if esquerda >= direita:
        return

    p = particiona(lista, esquerda, direita)
    print(lista)

    quick_sort(lista, p + 1, direita)
    quick_sort(lista, esquerda, p - 1)


def busca_em_listas(listas, elemento):
    if type(listas) == list:
        elemento1, elemento2 = listas
        esquerdo = busca_em_listas(elemento1, elemento)
        direito = busca_em_listas(elemento2, elemento)
        return esquerdo or direito
    else:
        return listas == elemento


# teste da busca binária
print(busca_binaria([1, 2, 3], 1))

# teste do quick sort
l = [1, 5, 3, 10]
print("Lista antes do quick sort =", l)
quick_sort(l)
print("Lista depois do quick sort =", l)

# teste da busca binária
print(busca_em_listas([[0, 1], [2, [3, 4]]], 10))
