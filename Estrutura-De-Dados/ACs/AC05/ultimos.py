# -*- coding: utf-8 -*-
"""
Created on Tue Apr  9 16:20:54 2019

@author: Alex Carneiro
"""


def enfileira(fila, elemento):
    fila.append(elemento)


def primeiro(fila):
    return fila[0]


def desenfileira(fila):
    return fila.pop(0)


def ultimos(fila, p):
    fila_aux = []
    lista_ultimos = []

    # transfere os elementos para a nova fila
    while len(fila) > 0:
        elemento = desenfileira(fila)
        enfileira(fila_aux, elemento)

        if len(fila) < p:
            lista_ultimos.append(elemento)

    # retorna os elementos para a fila original
    while len(fila_aux) > 0:
        elemento = desenfileira(fila_aux)
        enfileira(fila, elemento)

    return lista_ultimos


if __name__ == '__main__':
    n_letras = 26
    letras = [chr(ord('a') + i) for i in range(n_letras)]

    fila = []

    for i in range(n_letras//2):
        enfileira(fila, letras[i])
        enfileira(fila, letras[n_letras - i - 1])

    print("Fila de letras:", fila)

    ###########################################################################

    p = 3
    print("Últimos %d elementos da fila de letras:" % p, ultimos(fila, p))

    ###########################################################################

    p = 5
    print("Últimos %d elementos da fila de letras:" % p, ultimos(fila, p))

    ###########################################################################

    p = 12
    print("Últimos %d elementos da fila de letras:" % p, ultimos(fila, p))

    ###########################################################################

    p = 17
    print("Últimos %d elementos da fila de letras:" % p, ultimos(fila, p))
