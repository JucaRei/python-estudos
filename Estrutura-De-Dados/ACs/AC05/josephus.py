# -*- coding: utf-8 -*-
"""
Created on Tue Apr  9 16:30:14 2019

@author: Alex Carneiro
"""


def enfileira(fila, elemento):
    fila.append(elemento)


def primeiro(fila):
    return fila[0]


def desenfileira(fila):
    return fila.pop(0)


def josephus(jogadores, p):
    fila = []
    eliminados = []

    # podemos usar o for sobre os jogadores, porque 'jogadores' não é uma fila
    for jogador in jogadores:
        enfileira(fila, jogador)

    # o jogo avança até restar 1 jogador
    while len(fila) > 1:
        for i in range(p):
            enfileira(fila, desenfileira(fila))  # "passagem da bola"
        eliminados.append(desenfileira(fila))  # um jogador é eliminado

    ganhador = primeiro(fila)

    return ganhador, eliminados


if __name__ == '__main__':
    n = 10
    p = 3

    jogadores = list(range(n))

    print("Partida com %d jogadores, a bola avança %d pessoas" % (n, p))
    ganhador, eliminados = josephus(jogadores, p)
    print("%d foi o ganhador e os eliminados foram:" % ganhador, eliminados)

    ###########################################################################

    n = 15
    p = 3

    jogadores = list(range(n))

    print("Partida com %d jogadores, a bola avança %d pessoas" % (n, p))
    ganhador, eliminados = josephus(jogadores, p)
    print("%d foi o ganhador e os eliminados foram:" % ganhador, eliminados)

    ###########################################################################

    n = 10
    p = 14

    jogadores = list(range(n))

    print("Partida com %d jogadores, a bola avança %d pessoas" % (n, p))
    ganhador, eliminados = josephus(jogadores, p)
    print("%d foi o ganhador e os eliminados foram:" % ganhador, eliminados)
