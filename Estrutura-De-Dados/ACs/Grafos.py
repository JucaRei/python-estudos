# -*- coding: UTF-8 -*-
'''
    Exercícios
    
    -Implementar os algoritmos de busca em largura e busca em profundidade e testá-los com diferentes vértices iniciais.
    
    - Grafos para os testes:
    
    
Simulação online dos algoritmos de busca: 

https://visualgo.net/en/dfsbfs
    
    
g1 = {0:[1,2,3,4], 1:[0,2,3,4], 2:[0,1,3,4], 3:[0,1,2,4], 4:[0,1,2,3]} 
g2 = {0:[1,2,3,4], 1:[0,2,3], 2:[0,1,4], 3:[0,1,4], 4:[0,2,3]}
g3 = {0:[3,4,5], 1:[2,3,4], 2:[1,3,5], 3:[0,1,2], 4:[0,1,5], 5:[0,2,4]} 
g4 = {0:[1,3,5], 1:[0,2], 2:[1,3], 3:[0,2,4], 4:[3,5], 5:[0,1]}

'''
