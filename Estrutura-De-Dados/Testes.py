# -*- coding: UTF-8 -*-

'''Laço for '''
d = {'teste': 1, 'Ola': 2, 'Haha': 3}

for test in d:
    print(d[test])


for i in range(1, 20, 2):
    print(i)
'''Condicional if , else '''
num = 10

if (num % 2 == 0):  # se o resto da divisão do numero for igual a zero, o numero é par
    print('O número é par!')
else:
    print("O número é impar!")


'''Laço While'''
num = 0

while (num < 11):
    print(num)
    num = num + 1  # num vai receber ele mesmo + 1 , enquanto o laço não for satisfeito


'''Função def'''


def eh_par(num):
    if(num % 2 == 0):
        return True
    return False


print(eh_par(10))


'''Classe em python'''


class Pessoa:

    def __init__(self, nome, idade):   # é preciso ter o self em todos parâmetros
        self.nome = nome
        self.idade = idade

    def obterNome(self):
        return self.nome

    def obterIdade(self):
        return self.idade


p1 = Pessoa('Juca', 28)
p2 = Pessoa('Angelica', 48)
p3 = Pessoa('Paloma', 30)
print('Nome:  %s' % p1.obterNome())  # p.  acessar os metodos dessa pessoa
print('Idade:  %d' % p1.obterIdade())
# %s é um especificador de formato do tipo string
# %d é um especificador de formato do tipo inteiros

pessoas = []

pessoas.append(p1)  # adiciona ao final da lista
pessoas.append(p2)
pessoas.append(p3)

for pessoa in pessoas:
    print(pessoa.obterNome())


'''
Outro teste
'''


pares = [num for num in range(101) if (num % 2 == 0)]
print(pares)


letras = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','x','y','w','z']

lista = [lis for lis in letras[::2]]
print(lista)

"""
Lambdas
"""

def pot2(x):
    
    return x ** 2

pot2_ = lambda x: x**2

print(pot2(2))
print(pot2_(10))

"""
Fatorial
"""

# 5! = 5 * 4 * 3 * 2 * 1 = 
# 3! =  3 * 2 * 1 = 6
# n! = n * (n - 1) * (n - 2) ... 2 * 1

def fat(n):
    if(n == 0):
        return 1  # fatorial de 0 é 1
    return (n * fat(n - 1)) # recursivo

fat_ = lambda n: n * fat(n - 1) if n > 1 else 1   # retorna 1 porque fatorial de 0 é 1

print(fat_(7))
print(fat(5))

'''
MAP
'''
