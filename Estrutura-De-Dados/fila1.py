# -*- coding: UTF-8 -*-


class Fila:
    def __init__(self):
        self.fila = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','x','y','w','z']

    def inserir(self, n):
        self.fila.append(n)  # inseri o elemento no final

    def remover(self):
        if not self.vazia():    # so remove se a fila não estiver vazia
            return self.fila.pop(0)  # remove o desde o inicio da fila

    def tamanho(self):
        return len(self.fila)  # retorna o tamanho de fila

    def vazia(self):       # testar se a fila está vazia
        return self.tamanho() == 0


fila = Fila()
# letras = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','x','y','w','z']
print(fila.vazia())
print(fila)
print(fila.remover())
# print(fila.remover(3))
# print(fila.remover(5))
# print(fila.remover(7))
# print(fila.remover(9))
# print(fila.remover(11))
# print(fila.remover(13))
# print(fila.remover(15))
# print(fila.remover(17))
# print(fila.remover(19))
# print(fila.remover(21))
# print(fila.remover(23))
# print(fila.remover(25))

 
# fila.inserir(2)
# fila.inserir(3)
print(fila.tamanho())
# print(fila.remover())
# print(fila.remover())
# print(fila.remover())
