# -*- coding: UTF-8 -*-

def enqueue(fila, elementos): 
    """Adiciona um elemento à fila"""
    fila.append(170)
fila = [110,120,130,140,150,160]

def dequeue(fila):
    """Remove e retorna um elemento da fila""" 
    return fila.pop(0)



def first(fila):
    fila = [10,20,30,40,50,60]
    """Retorna o primeiro elemento da fila"""
    return fila[0]


def length(fila):
    """Retorna o número de elementos da fila""" 
    return len(fila)


class Fila1(object):
    def __init__(self):
        self.dados = [100,110,120,130,140,150]
 
    def insere(self, elemento):
        self.dados.append(elemento)
 
    def retira(self):
        return self.dados.pop(0)
 
    def vazia(self):
        return len(self.dados) == 0


class Fila:
    def __init__(self):
        self.fila = []
    def inserir(self, n):
        self.fila.append(n)  #inseri o elemento no final
    def excluir(self):
        if not self.vazia():  # verifica se não esta vazia
            return self.fila.pop(0)  #excluir do inicio // retorna os elementos excluidos
    def tamanho(self):
        return len(self.fila)  # retorna o tamanho da fila , o retorno de len
    def vazia(self):
        return self.tamanho() == 0  # retorna se esta vazia ou não

fila = Fila()
fila.inserir(1)
fila.inserir(2)
fila.inserir(3)
print(fila.excluir())
print(fila.excluir())
print(fila.excluir())
#print(fila.tamanho())
print(fila.vazia())

