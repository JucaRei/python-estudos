# Reinaldo Ponce Junior   RA: 1020056

def verifica_ordenacao(list):
    x = 1
    while x < len(list):
        if list[x-1] > list[x]:
            return False
        x = x + 1
    return True
        
    
def busca(list, elemento_desejado):
    for x in range(len(list)):
        if elemento_desejado == list[x]:
            return (x)
    return None 

# sempre faz as verificação das pontas 
# da esquerda começa no indice 0, entao esquerda = 0
# da direita é o tamanho da lista - 1 , porque sempre começa de 0 a lista, entao direita = len(tamanholista) - 1
# busca binaria tem que sempre estar ordenado
# busca binaria sempre divide a lista, então : direita + esquerda // 2 
def busca_binaria(list, elemento_desejado):
    a = 0
    c = len(list)-1
    while a <= c:
        b = (a+c)//2
        if elemento_desejado == list[b]:
            return b
        elif elemento_desejado > (list[b]):
            a = b +1
        else:
            c = b -1
        #return -1

def busca_binaria_strings(list, elemento_desejado):
    x = 1
    while x < len(list):
        if list[x-1] > list[x]:
            return None
        x = x + 1
    a = 0
    c = len(list)-1
    while a <= c:
        b = (a+c)//2
        if elemento_desejado == list[b]:
            return b
        elif elemento_desejado > (list[b]):
            a = b +1
        else:
            c = b -1

list = ['abc', 'abd', 'bola', 'casa', 'casas']            
print (verifica_ordenacao(list))
print (busca(list,'bola'))
print(busca_binaria(list,'bola'))
print(busca_binaria_strings(list,'casa'))
