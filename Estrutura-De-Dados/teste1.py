# -*- coding: UTF-8 -*-

def max3 (a,b,c) :
    if a >= b and a >= c :
        return a
    elif b >= a and b >= c :
        return b
    else:
        return c

max2 = lambda a,b : a if a > b else b

def max3v2 (a,b,c):
    return max2(max2(a,b), c)


lista = [2,56,1,2,7]

def min_lista(lista):
    menor = lista[0]

    for elemento in lista:
        if elemento < menor:
            menor = elemento
    return menor

print("Menor elemento da lista", lista, "=", min_lista(lista))

def is_list(obj):
    return type(obj) == list

def soma_primeiro_ultimo(lista):
    return lista[0] + lista[-1]  # contagem reversa [-1] é o ultimo elemento
print(soma_primeiro_ultimo(lista))

def mean(lista):
    soma = 0

    for elemento in lista:
        soma += elemento
    return soma/len (lista)

print(mean(lista))

lista1 = [1,5,'a','abc',0,-2,'xyz']
def extrair_strings(lista1):
    strings = []
    for elementos in lista1:
        if type(elementos) == str:
            strings.append(elementos)
    return strings

extract_str = lambda lista: [x for x in lista if type(x)==str]

print("Strings da lista",lista, '= ' ,extrair_strings(lista1))
print("Strings da lista",lista, '= ' ,extract_str(lista1))

# python é orientado a objetos

#  Bibliotecas Externas
#  - OS     - Serve para acessar recursos do sistema Operacional(interface para coleta de informações do Sistema Operacional)
#  - Math   - Funções matemáticas
#  - Numpy   - Funções matematicas , mas com foco em algebra linear
#  - Pandas   - analise de dados , leitura e escrita de arquivos
#  -Matplotlib   -  geração de gráficos
