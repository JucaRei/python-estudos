# -*- coding: UTF-8 -*-

#Uso da tupla como TAD para as peças de dominó
#verifica se é uma tupla, tem que ter 2 elementos ,se os 2 primeiros elementos såo inteiros, estão no intervalo de 0 a 6

def verifica(p):    
    return type(p)==tuple and\
           len(p)==2 and\
           type(p[0])==int and\
           type(p[1])==int and\
           0 <= p[0] <= 6 and\
           0 <= p[1] <= 6

def conecta(p1,p2):
    return p1[1]==p2[0]

def ler_arquivo(nome_do_arquivo, i):
    #Faz a leitura da linha de posição i no arquivo
    with open(nome_do_arquivo, 'r') as arquivo:
        linhas = arquivo.readlines()

    linha = linhas[i]
    peca = linha.replace('\n','').split(',')
    return tuple([int(x) for x in peca])

def len_arquivo(nome_do_arquivo):
    #Retorna a quantidade de linhas do arquivo
    with open(nome_do_arquivo, 'r') as arquivo:
        linhas = arquivo.readlines()

    return len(linhas)

def verifica_sequencia(seq):
    #Verifica se todas as peças são válidas
    for peca in seq:
        if not verifica(peca):
            return False

    #Verifica se todas as peças estão corretamente conectadas
    for i in range(1,len(seq)):
        if not conecta(seq[i-1], seq[i]):
            return False
        
    return True

def verifica_circular(seq):
    #Verifica se a sequência é válida e se a última peça conecta à primeira
    return verifica_sequencia(seq) and conecta(seq[-1],seq[0])

def verifica_repetidas(seq):
    #Verifica se existe mais de uma ocorrência de cada peça ou se a peça ocorre invertida, exemplo: (1,2) e (2,1)
    for peca in seq:
        if seq.count(peca) > 1 or (seq.count(peca[::-1]) > 0 if peca[0] != peca[1] else False):
            return True
    return False

if __name__ == '__main__':
    arquivo='domino.dat'

    #cria a sequência de peças
    sequencia=[]
    for i in range(len_arquivo(arquivo)):
        sequencia.append(ler_arquivo(arquivo,i))

    print('Sequencia =',sequencia)

    if verifica_sequencia(sequencia):
        print('A sequência é válida')
    else:
        print('A sequência não é válida')

    if verifica_circular(sequencia):
        print('A sequência é circular')
    else:
        print('A sequência não é circular')

    if verifica_repetidas(sequencia):
        print('A sequência possui peças repetidas')
    else:
        print('A sequência não possui peças repetidas')