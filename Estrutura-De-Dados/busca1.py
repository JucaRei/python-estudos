#coding: utf-8

#Busca Linear
lista = [1,2,3,4,5,6,7]
def busca(lista, elemento_desejado=int):
    for indice in range(len(lista)):    # no tamanho da lista
        elemento = lista[indice]
    if elemento == elemento_desejado:
        return indice
    return None
print(busca(lista))

# [105, 121, 147, 183, 229, 285, 351, 427, 513]


''' Este processo de utilizar o elemento central e verificar se o elemento desejado é igual,
 maior ou menor é denominado busca binária.

O termo busca binária decorre do fato de que temos sempre 2 possibilidades de continuar
 a busca quando não encontramos o elemento desejado:
-à esquerda
-à direita

Busca binário consistem em manipular indices
'''

#[‘abc’, ‘abd’, ‘bola’, ‘casa’, ‘casas’]

def pesquisa_binaria(lista, item):
    numero_mais_baixo = 0               #lembrando que 1 array sempre começa do indice 0
    numero_mais_alto = len(lista) -1       # como o indice de 1 array começa de 0, então o ultimo indice é o total -1 (por ex: 1 à 100, indice 100 - 1 = 99, ultimo indice é 99)  

    while numero_mais_baixo <= numero_mais_alto:
        meio_da_lista = (numero_mais_baixo + numero_mais_alto) /2
        chute  = lista[meio_da_lista]
        if chute == item:
            return meio_da_lista
        if chute > item:
            numero_mais_alto = meio_da_lista - 1
        else:
            numero_mais_baixo = meio_da_lista + 1
    return None

minha_lista = [1,2,3,4,6,7,12,23,31,35,76,77,88,90]
print(pesquisa_binaria(minha_lista, 23))

def pesquisa_binaria1(lista, item):
    numero_mais_baixo = 0               #lembrando que 1 array sempre começa do indice 0
    numero_mais_alto = len(lista) -1       # como o indice de 1 array começa de 0, então o ultimo indice é o total -1 (por ex: 1 à 100, indice 100 - 1 = 99, ultimo indice é 99)  

    while numero_mais_baixo <= numero_mais_alto:
        meio_da_lista = (numero_mais_baixo + numero_mais_alto) /2
        chute  = lista[meio_da_lista]
        if chute == item:
            return meio_da_lista
        if chute > item:
            numero_mais_alto = meio_da_lista - 1
        else:
            numero_mais_baixo = meio_da_lista + 1
    return None

minha_lista = [100,102,104,106,108,110,112,114,116,118,120,122,124,126,128,130,132,134,136,138,140,142,144,146,148,150,152,154,156,158,160,162,164,166,168,170,172,174,176,178,180,182,184,186,188,190,192,194,196,198,200,]
#minha_lista = ['abc', 'abd', 'bola', 'casa', 'casas']
print(pesquisa_binaria1(minha_lista, 122))