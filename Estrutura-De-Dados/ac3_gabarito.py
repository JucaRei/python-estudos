# -*- coding: UTF-8 -*-
def verifica_ordenacao(lista):
    """Verifica se a lista está ordenada e retorna True se a lista estiver ordenada ou False se a lista não tiver ordenada"""
    for i in range(1,len(lista)):
        if lista[i] < lista[i-1]:
            return False
    return True


def busca(lista, elemento_desejado):
    """Busca linear que retorna o índice do elemento desejado se estiver na lista, caso contrário retorna None"""
    for i in range(len(lista)):
        elemento = lista[i]
        if elemento == elemento_desejado:
            return i
    return None


def busca_binaria(lista, elemento_desejado):
    """Busca binária que retorna o índice do elemento desejado se estiver na lista, caso contrário retorna None"""
    indice_esquerda = 0
    indice_direita = len(lista) - 1

    while indice_esquerda <= indice_direita:
        indice_centro = (indice_esquerda + indice_direita)//2
        elemento = lista[indice_centro]

        if elemento == elemento_desejado:
            return indice_centro
        elif elemento > elemento_desejado:
            indice_direita = indice_centro - 1
        else:
            indice_esquerda = indice_centro + 1

    return None


def busca_binaria_strings(lista, elemento_desejado):
    """Busca binária com uma lista de strings que retorna o índice do elemento desejado se estiver na lista, caso contrário retorna None. Se a lista não estiver ordenada, também retorna None"""
    if not verifica_ordenacao(lista):
        return None

    return busca_binaria(lista, elemento_desejado)

if __name__ == '__main__':
    """Testes com as funções"""
    lista1 = [3,6,6,34,42,2,56,6,5,2,12]
    lista2 = [0,6,16,34,42,52,56,66,75,82,112]
    lista3 = [-3,-6,6,34,42,52,56,76,85,92,112]
    lista4 = [-100,-50,-10,0,1,17,38,100]

    elementos = [0, -3, 5, 6, 12, 100]

    print("#######################################")
    print("# Teste da função verifica_ordenacao()#")
    print("#######################################")
    if verifica_ordenacao(lista1):
        print("Lista:",lista1,"está ordenada")
    else:
        print("Lista:",lista1,"não está ordenada")

    if verifica_ordenacao(lista2):
        print("Lista:",lista2,"está ordenada")
    else:
        print("Lista:",lista2,"não está ordenada")

    if verifica_ordenacao(lista3):
       print("Lista:",lista3,"está ordenada")
    else:
        print("Lista:",lista3,"não está ordenada")

    if verifica_ordenacao(lista4):
       print("Lista:",lista4,"está ordenada")
    else:
        print("Lista:",lista4,"não está ordenada")

    print()
    print("##############################################")
    print("# Teste das funções de busca linear e binária#")
    print("##############################################")
    for e in elementos:
        p_linear = busca(lista1,e)
        p_bin = busca_binaria(lista1,e) if verifica_ordenacao(lista1) else p_linear

        if p_linear is not None and p_bin is not None:
            if p_linear == p_bin:
                print("Elemento",e,"está no índice",p_bin,"na lista",lista1)
            else:
                print("Buscas linear e binária inconsistentes para o elemento",e,"na lista",lista1)
        else:
            print("O elemento",e,"não está na lista",lista1)

    for e in elementos:
        p_linear = busca(lista2,e)
        p_bin = busca_binaria(lista2,e) if verifica_ordenacao(lista2) else p_linear

        if p_linear is not None and p_bin is not None:
            if p_linear == p_bin:
                print("Elemento",e,"está no índice",p_bin,"na lista",lista2)
            else:
                print("Buscas linear e binária inconsistentes para o elemento",e,"na lista",lista2)
        else:
            print("O elemento",e,"não está na lista",lista2)

    for e in elementos:
        p_linear = busca(lista3,e)
        p_bin = busca_binaria(lista3,e) if verifica_ordenacao(lista3) else p_linear

        if p_linear is not None and p_bin is not None:
            if p_linear == p_bin:
                print("Elemento",e,"está no índice",p_bin,"na lista",lista3)
            else:
                print("Buscas linear e binária inconsistentes para o elemento",e,"na lista",lista3)
        else:
            print("O elemento",e,"não está na lista",lista3)

    for e in elementos:
        p_linear = busca(lista4,e)
        p_bin = busca_binaria(lista4,e) if verifica_ordenacao(lista4) else p_linear

        if p_linear is not None and p_bin is not None:
            if p_linear == p_bin:
                print("Elemento",e,"está no índice",p_bin,"na lista",lista4)
            else:
                print("Buscas linear e binária inconsistentes para o elemento",e,"na lista",lista4)
        else:
            print("O elemento",e,"não está na lista",lista4)

    print()
    print("#################################################")
    print("# Teste das funções de busca binária com strings#")
    print("#################################################")
    lista1 = ['abc', 'abcd', 'abdc', 'abe', 'azer', 'azir', 'azor', 'azur']
    lista2 = ['abc', 'xabcd', 'abdc', 'abe', 'azer', 'azir', 'azor', 'azur']

    e = 'abc'
    p = busca_binaria_strings(lista1, e)
    if p is not None:
        print("A string",e,"está no índice",p,"da lista", lista1)
    else:
        print("A string",e,"não está na lista", lista1,"ou esta não está ordenada")

    e = 'azeite'
    p = busca_binaria_strings(lista1, e)
    if p is not None:
        print("A string",e,"está no índice",p,"da lista", lista1)
    else:
        print("A string",e,"não está na lista", lista1,"ou esta não está ordenada")

    e = 'abc'
    p = busca_binaria_strings(lista2, e)
    if p is not None:
        print("A string",e,"está no índice",p,"da lista", lista2)
    else:
        print("A string",e,"não está na lista", lista2,"ou esta não está ordenada")

    e = 'azeite'
    p = busca_binaria_strings(lista1, e)
    if p is not None:
        print("A string",e,"está no índice",p,"da lista", lista2)
    else:
        print("A string",e,"não está na lista", lista2,"ou esta não está ordenada")
