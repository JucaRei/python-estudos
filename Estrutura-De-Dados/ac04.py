#coding: utf-8

#Busca Binaria
def pesquisa_binaria1(lista, item):
    numero_mais_baixo = 0
    numero_mais_alto = len(lista) - 1

    while numero_mais_baixo <= numero_mais_alto:
        meio_da_lista = (numero_mais_baixo + numero_mais_alto) / 2
        chute = lista[meio_da_lista]
        if chute == item:
            return meio_da_lista
        if chute > item:
            numero_mais_alto = meio_da_lista - 1
        else:
            numero_mais_baixo = meio_da_lista + 1
    return None


lista = [100, 102, 104, 106, 108, 110, 112, 114, 116, 118, 120, 122, 124, 126, 128, 130, 132, 134, 136, 138, 140, 142, 144, 146,
               148, 150, 152, 154, 156, 158, 160, 162, 164, 166, 168, 170, 172, 174, 176, 178, 180, 182, 184, 186, 188, 190, 192, 194, 196, 198, 200, ]
print(pesquisa_binaria1(lista, 122))

#Busca Linear
def busca_linear(lista, elemento_desejado):
    for x in range(len(lista)):
        print(lista[x])
        if elemento_desejado == lista[x]:
            return (x)
    return None 

minha_lista2 = [100, 102, 104, 106, 108, 110, 112, 114, 116, 118, 120, 122, 124, 126, 128, 130, 132, 134, 136, 138, 140, 142, 144, 146,
               148, 150, 152, 154, 156, 158, 160, 162, 164, 166, 168, 170, 172, 174, 176, 178, 180, 182, 184, 186, 188, 190, 192, 194, 196, 198, 200, ]

print(busca_linear(minha_lista2, 122))