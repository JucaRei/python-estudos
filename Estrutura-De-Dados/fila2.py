# -*- coding: UTF-8 -*-
class Queue:

    def __init__(self):
        self.queue = []  # fila vazia
        self.len_queue = 0  # tamanho da fila

    def push(self, e):
        self.queue.append(e)
        self.len_queue += 1

    # Teste 2
    # def push2(self, e):
    #     self.queue.append(e)
    #     self.len_queue += 2

    def pop(self):
        if not self.empty():
            self.queue.pop(0)
            self.len_queue -= 1

    def empty(self):        # não faz nada
        if self.len_queue == 0:
            return True
        else:
            return False

    def length(self):
        return self.len_queue

    def front(self):
        if not self.empty():
            return self.queue[0]

    def last(self):
        if not self.empty():
            return self.queue[-1]
        return None

    def show(self):
        for i in self.queue:
            print(i)


q = Queue()
q.push('a')
q.push('b')
q.push('c')
q.push('d')
q.push('e')
q.push('f')
  # prints True
print(q.enqueue('a'))  
print(q.enqueue('b')) 
print(q.enqueue('c'))  
print(q.enqueue('d'))  
print(q.enqueue('e')) 
print(q.enqueue('f')) 
print(q.enqueue('g')) 
print(q.enqueue('h')) 
print(q.enqueue('i')) 
''' ================================= '''
# q.push2('a')
# q.push2('b')
# q.push2('c')
# q.push2('d')
# q.push2('e')
# q.push2('f')
print(q.length())
print('========================================')
print(q.front())
print('========================================')
print(q.pop())
print('========================================')
print(q.last())
print('========================================')
print(q.show())
